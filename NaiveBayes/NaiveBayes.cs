﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NaiveBayes
{
    public class NaiveBayes<TFeature, TClass>
    {
        private HashSet<TClass> _allClasses;

        private HashSet<TFeature> _allFeatures;
        private Dictionary<TClass, double> _classPossibilities;
        private Dictionary<TFeature, Dictionary<TClass, double>> _possibility;


        public TClass Predict(IList<TFeature> input)
        {
            var maxP = double.MinValue;
            var maxClass = default(TClass);
            foreach (var cl in _allClasses)
            {
                var p = 1.0;
                foreach (var feature in input)
                {
                    if (_possibility.ContainsKey(feature))
                        p *= _possibility[feature][cl];
                }
                p *= _classPossibilities[cl];

                if (p > maxP)
                {
                    maxP = p;
                    maxClass = cl;
                }
            }

            return maxClass;
        }

        public void Train(IList<Entity<TFeature, TClass>> trainingSet)
        {
            _allFeatures = new HashSet<TFeature>();
            _allClasses = new HashSet<TClass>();
            _possibility = new Dictionary<TFeature, Dictionary<TClass, double>>();
            foreach (var tuple in trainingSet)
            {
                foreach (var feature in tuple.Data)
                {
                    if (_allFeatures.Contains(feature))
                        continue;
                    _allFeatures.Add(feature);
                }
                if (_allClasses.Contains(tuple.Class))
                    continue;
                _allClasses.Add(tuple.Class);
            }


            foreach (var feature in _allFeatures)
                _possibility[feature] = _allClasses.Select(x => new
                {
                    cl = x,
                    p = GetPossibility(trainingSet, feature, x)
                }).ToDictionary(x => x.cl, x => x.p);


            _classPossibilities = _allClasses.Select(x =>
                new Tuple<TClass, double>(x,
                    (double)trainingSet.Count(sample => sample.Class.Equals(x)) / trainingSet.Count))
                .ToDictionary(x => x.Item1, x => x.Item2);
        }

        private double GetPossibility(ICollection<Entity<TFeature, TClass>> data, TFeature feature, TClass cl)
        {
            var pc = data.Where(x => x.Class.Equals(cl)).Count(x => x.Contains(feature));

            if (pc == 0)
            {
                return 1.0 / data.Count;
            }

            return (double)pc / data.Count(x => x.Class.Equals(cl));
        }
    }
}