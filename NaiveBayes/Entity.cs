﻿using System.Collections.Generic;

namespace NaiveBayes
{
    public abstract class Entity<TFeature, TClass>
    {
        private readonly HashSet<TFeature> _features;

        protected Entity()
        {
        }

        protected Entity(IEnumerable<TFeature> data, TClass cl)
        {
            Class = cl;
            _features = new HashSet<TFeature>();
            Data = new List<TFeature>(data);
            foreach (var feature in Data)
                _features.Add(feature);
        }

        public IList<TFeature> Data { get; set; }
        public TClass Class { get; set; }

        public bool Contains(TFeature feature)
        {
            return _features.Contains(feature);
        }
    }
}