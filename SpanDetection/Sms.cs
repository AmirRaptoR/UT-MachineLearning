using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using NaiveBayes;

namespace SpanDetection
{
    public class Sms : Entity<string, bool>
    {
        public static readonly char[] Separators = { ' ', ',', '.', '?' };

        public Sms(string line, ICollection<string> stopWords) : base(ExtractWords(line, stopWords), ExtractClass(line))
        {

        }

        private static bool ExtractClass(string line)
        {
            return line.Substring(1, 3) == "spa";
        }

        private static string[] ExtractWords(string line, ICollection<string> stopWords)
        {
            var sms = line.Substring(line.IndexOf("\"", 6, StringComparison.Ordinal) + 1);
            sms = sms.Substring(0, sms.Length - 1);
            var rgx = new Regex("[^a-zA-Z -]");
            sms= rgx.Replace(sms, " ");
            return
                sms.Split(Separators, StringSplitOptions.RemoveEmptyEntries)
                    .Where(w => !stopWords.Contains(w))
                    .ToArray();
        }
    }
}