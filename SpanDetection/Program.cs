﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using NaiveBayes;

namespace SpanDetection
{
    internal class Program
    {
        private const string StopWordsFileName = "stopwords.csv";
        private const string DataFile = "sms_spam.csv";

        private static void Main(string[] args)
        {
            Console.WriteLine("Starting");

            Console.WriteLine("Reading stopwords");
            var stopWords = Extensions.FillStopWords(StopWordsFileName);

            Console.WriteLine("Reading data");
            Sms[] data = Extensions.FillData(DataFile, stopWords);

            var random = new Random();

            const int iterations = 10;
            double accuracy = 0;

            for (int i = 0; i < iterations; i++)
            {
                Console.WriteLine($"=====================================Iteration  {i + 1} / {iterations}");


                var alg = new NaiveBayes<string, bool>();

                Console.WriteLine("Shuffling dataset");
                data = data.OrderBy(x => random.Next(data.Length)).ToArray();
                var trainingSet = data.Take(4 * data.Length / 5).ToArray();
                Console.WriteLine("Training ... ");
                alg.Train(trainingSet);


                var testData = data.Skip(4 * data.Length / 5);

                int total = 0, falsePositive = 0, falseNegative = 0, truePositive = 0, trueNegative = 0;
                foreach (var smse in testData)
                {
                    total++;
                    var cl = alg.Predict(smse.Data);

                    if (smse.Class == cl)
                    {
                        if (smse.Class)
                            truePositive++;
                        else
                            trueNegative++;
                        continue;
                    }

                    if (smse.Class)
                        falseNegative++;
                    else
                        falsePositive++;
                }

                var acc = (double)(trueNegative + truePositive) / total;

                Console.WriteLine("\t\t|\tSPAM\t|\tHAM\t");
                Console.WriteLine($"SPAM\t\t|\t{truePositive}\t|\t{falsePositive}\t");
                Console.WriteLine($"HAM\t\t|\t{falseNegative}\t|\t{trueNegative}\t");

                Console.WriteLine($"Result : accuracy - {acc}");

                accuracy += acc;

            }

            Console.WriteLine("========================================");
            Console.WriteLine($"Final Result : accuracy - {accuracy}");
            Console.ReadKey();

        }
    }
}