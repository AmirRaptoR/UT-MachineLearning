using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using NaiveBayes;

namespace SpanDetection
{
    public static class Extensions
    {
        public static void Increment<T>(this Dictionary<T, int> dic, T key)
        {
            if (dic.ContainsKey(key))
                dic[key] = dic[key] + 1;
            else
                dic[key] = 1;
        }

        public static void Increment<T>(this ConcurrentDictionary<T, int> dic, T key)
        {
            if (dic.ContainsKey(key))
                dic[key] = dic[key] + 1;
            else
                dic[key] = 1;
        }

        public static HashSet<string> FillStopWords(string file)
        {
            var stopWords = new HashSet<string>();
            using (var reader = new StreamReader(file))
            {
                while (!reader.EndOfStream)
                {
                    var str = reader.ReadLine();
                    if (string.IsNullOrWhiteSpace(str))
                        continue;
                    var lst = str.Split(new[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var s in lst)
                        stopWords.Add(s);
                }
            }
            return stopWords;
        }

        public static Sms[] FillData(string fileName, HashSet<string> stopWords)
        {
            var datas = File.ReadAllLines(fileName);
            var lst = new ConcurrentBag<Sms>();
            Parallel.For(1, datas.Length, i =>
            {
                var str = datas[i].ToLower();
                if (string.IsNullOrWhiteSpace(str))
                    return;
                var sms = new Sms(str, stopWords);
                lst.Add(sms);
            });
            return lst.ToArray();
        }
    }
}